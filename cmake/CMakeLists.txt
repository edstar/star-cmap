# Copyright (C) 2020, 2021 |Meso|Star> (contact@meso-star.com)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

cmake_minimum_required(VERSION 3.1)
project(scmap C)
enable_testing()

set(SCMAP_SOURCE_DIR ${PROJECT_SOURCE_DIR}/../src)
option(NO_TEST "Do not build tests" OFF)

################################################################################
# Check dependencies
################################################################################
find_package(RCMake 0.4 REQUIRED)
find_package(RSys 0.9 REQUIRED)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${RCMAKE_SOURCE_DIR})
include(rcmake)
include(rcmake_runtime)

include_directories(${RSys_INCLUDE_DIR})

################################################################################
# Configure and define targets
################################################################################
set(VERSION_MAJOR 0)
set(VERSION_MINOR 0)
set(VERSION_PATCH 3)
set(VERSION ${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH})

set(SCMAP_FILES_SRC scmap.c scmap_palettes.c)
set(SCMAP_FILES_INC )
set(SCMAP_FILES_INC_API scmap.h)
set(SCMAP_FILES_DOC COPYING README.md)

# Prepend each file in the `SCMAP_FILES_<SRC|INC>' list by `SCMAP_SOURCE_DIR'
rcmake_prepend_path(SCMAP_FILES_SRC ${SCMAP_SOURCE_DIR})
rcmake_prepend_path(SCMAP_FILES_INC ${SCMAP_SOURCE_DIR})
rcmake_prepend_path(SCMAP_FILES_INC_API ${SCMAP_SOURCE_DIR})
rcmake_prepend_path(SCMAP_FILES_DOC ${PROJECT_SOURCE_DIR}/../)

add_library(scmap SHARED ${SCMAP_FILES_SRC} ${SCMAP_FILES_INC} ${SCMAP_FILES_INC_API})
target_link_libraries(scmap RSys)

set_target_properties(scmap PROPERTIES
  DEFINE_SYMBOL SCMAP_SHARED_BUILD
  VERSION ${VERSION}
  SOVERSION ${VERSION_MAJOR})

rcmake_setup_devel(scmap StarCMap ${VERSION} star/scmap_version.h)

################################################################################
# Add tests
################################################################################
if(NOT NO_TEST)
  function(build_test _name)
    add_executable(${_name} ${SCMAP_SOURCE_DIR}/${_name}.c)
    target_link_libraries(${_name} scmap RSys)
  endfunction()

  function(new_test _name)
    build_test(${_name})
    add_test(${_name}_${_palette} ${_name})
  endfunction()

  new_test(test_scmap)
  new_test(test_scmap_fetch_color)

  build_test(test_scmap_palettes)
  add_test(test_scmap_palettes_accent test_scmap_palettes "accent")
  add_test(test_scmap_palettes_blues test_scmap_palettes "blues")
  add_test(test_scmap_palettes_brbg test_scmap_palettes "brbg")
  add_test(test_scmap_palettes_bugn test_scmap_palettes "bugn")
  add_test(test_scmap_palettes_bupu test_scmap_palettes "bupu")
  add_test(test_scmap_palettes_chromajs test_scmap_palettes "chromajs")
  add_test(test_scmap_palettes_dark2 test_scmap_palettes "dark2")
  add_test(test_scmap_palettes_gnbu test_scmap_palettes "gnbu")
  add_test(test_scmap_palettes_gnpu test_scmap_palettes "gnpu")
  add_test(test_scmap_palettes_greens test_scmap_palettes "greens")
  add_test(test_scmap_palettes_greys test_scmap_palettes "greys")
  add_test(test_scmap_palettes_inferno test_scmap_palettes "inferno")
  add_test(test_scmap_palettes_jet test_scmap_palettes "jet")
  add_test(test_scmap_palettes_magma test_scmap_palettes "magma")
  add_test(test_scmap_palettes_moreland test_scmap_palettes "moreland")
  add_test(test_scmap_palettes_oranges test_scmap_palettes "oranges")
  add_test(test_scmap_palettes_orrd test_scmap_palettes "orrd")
  add_test(test_scmap_palettes_paired test_scmap_palettes "paired")
  add_test(test_scmap_palettes_parula test_scmap_palettes "parula")
  add_test(test_scmap_palettes_pastel1 test_scmap_palettes "pastel1")
  add_test(test_scmap_palettes_pastel2 test_scmap_palettes "pastel2")
  add_test(test_scmap_palettes_piyg test_scmap_palettes "piyg")
  add_test(test_scmap_palettes_plasma test_scmap_palettes "plasma")
  add_test(test_scmap_palettes_prgn test_scmap_palettes "prgn")
  add_test(test_scmap_palettes_pubu test_scmap_palettes "pubu")
  add_test(test_scmap_palettes_pubugn test_scmap_palettes "pubugn")
  add_test(test_scmap_palettes_puor test_scmap_palettes "puor")
  add_test(test_scmap_palettes_purd test_scmap_palettes "purd")
  add_test(test_scmap_palettes_purples test_scmap_palettes "purples")
  add_test(test_scmap_palettes_rdbu test_scmap_palettes "rdbu")
  add_test(test_scmap_palettes_rdgy test_scmap_palettes "rdgy")
  add_test(test_scmap_palettes_rdpu test_scmap_palettes "rdpu")
  add_test(test_scmap_palettes_rdylbu test_scmap_palettes "rdylbu")
  add_test(test_scmap_palettes_rdylgn test_scmap_palettes "rdylgn")
  add_test(test_scmap_palettes_reds test_scmap_palettes "reds")
  add_test(test_scmap_palettes_sand test_scmap_palettes "sand")
  add_test(test_scmap_palettes_set1 test_scmap_palettes "set1")
  add_test(test_scmap_palettes_set2 test_scmap_palettes "set2")
  add_test(test_scmap_palettes_set3 test_scmap_palettes "set3")
  add_test(test_scmap_palettes_spectral test_scmap_palettes "spectral")
  add_test(test_scmap_palettes_viridis test_scmap_palettes "viridis")
  add_test(test_scmap_palettes_whgnbu test_scmap_palettes "whgnbu")
  add_test(test_scmap_palettes_whylrd test_scmap_palettes "whylrd")
  add_test(test_scmap_palettes_ylgn test_scmap_palettes "ylgn")
  add_test(test_scmap_palettes_ylgnbu test_scmap_palettes "ylgnbu")
  add_test(test_scmap_palettes_ylorbr test_scmap_palettes "ylorbr")
  add_test(test_scmap_palettes_ylorrd test_scmap_palettes "ylorrd")
  add_test(test_scmap_palettes_ylrd test_scmap_palettes "ylrd")

  if(CMAKE_COMPILER_IS_GNUCC) 
    target_link_libraries(test_scmap_fetch_color m)
  endif()

endif()

################################################################################
# Define output & install directories
################################################################################
install(TARGETS scmap
  ARCHIVE DESTINATION bin
  LIBRARY DESTINATION lib
  RUNTIME DESTINATION bin)
install(FILES ${SCMAP_FILES_INC_API} DESTINATION include/star)
install(FILES ${SCMAP_FILES_DOC} DESTINATION share/doc/star-cmap)


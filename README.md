# Star-ColorMap

This library maps a a real value to a color by interpolating between
pre-defined support points defined in palettes. Several common palettes are
provided by the library.

## How to build

Star-ColorMap is compatible GNU/Linux 64 bits. It relies on the
[CMake](http://www.cmake.org) and the
[RCMake](https://gitlab.com/vaplv/rcmake/) packages to build.  It also depends
on the [RSys](https://gitlab.com/vaplv/rsys/), library.

First ensure that CMake is installed on your system. Then install the RCMake
package as well as the RSys prerequisite. Finally generate the project from the
`cmake/CMakeLists.txt` file by appending to the `CMAKE_PREFIX_PATH` variable
the install directories of its dependencies. The resulting project can be
edited, built, tested and installed as any CMake project. Refer to the [CMake
documentation](https://cmake.org/documentation) for further informations on
CMake.

## Release notes

### Version 0.0.3

Fix compilation warnings detected by gcc 11 

### Version 0.0.2

Sets the CMake minimum version to 3.1: since CMake 3.20, version 2.8 has become
obsolete.

### Version 0.0.1

Fix MSVC build.

## License

Copyright (C) 2020, 2021 [|Meso|Star>](https://www.meso-star.com)
(<contact@meso-star.com>). Star-ColorMap is free software released
under the GPL v3+ license: GNU GPL version 3 or later. You are welcome to
redistribute it under certain conditions; refer to the COPYING file for
details.
